----------------- General Data -------------------------
Name: Public Books
Context: /books
version: 1.0.0
Description: This is a RESTFul API for get public books

------------------  Business Information  -------------
Business Owner
Musala

Technical Owner
Frank Nicolau Gonzalez

Business Owner email
musalabookstore@gmail.com

Technical Owner email
frank.nicolau03@gmail.com

---------------- Endpoints -----------------------------
Production
http://localhost:8282/books
sandbox
http://localhost:8282/books


