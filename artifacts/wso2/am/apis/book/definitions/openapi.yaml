---
  openapi: "3.0.1"
  info: 
    title: "Book"
    version: "1.0.0"
  servers: 
    - 
      url: "/"
  security: 
    - 
      default: []
  paths: 
    /books: 
      get: 
        summary: "Get book by id"
        description: "Return book by id"
        parameters: 
          - 
            name: "id"
            in: "query"
            required: true
            style: "form"
            explode: true
            schema: 
              type: "integer"
        responses: 
          200: 
            description: "A user object."
            content: 
              application/json: 
                schema: 
                  $ref: "#/components/schemas/Book"
                examples: 
                  Book: 
                    value: 
                      id: 10
                      title: "WSO2 Api Managament in Action"
                      autor: "Frank E Nicolau Gonzalez"
                      price: 790
                      published: 1
                      userName: "frank"
                      fileUri: "/frank/wso2inaction.pdf"
          404: 
            description: "Book not found"
            content: 
              application/json: 
                schema: 
                  $ref: "#/components/schemas/Error"
          500: 
            description: "Internal server error"
            content: 
              application/json: 
                schema: 
                  $ref: "#/components/schemas/Error"
        security: 
          - 
            default: []
        x-auth-type: "Application & Application User"
        x-throttling-tier: "Unlimited"
      put: 
        summary: "Update book"
        requestBody: 
          content: 
            application/json: 
              schema: 
                $ref: "#/components/schemas/Book"
              examples: 
                Book: 
                  value: 
                    id: 10
                    title: "Api Managament in Action"
                    autor: "Frank E Nicolau Gonzalez"
                    price: 790
                    published: 1
                    userName: "frank"
        responses: 
          200: 
            description: "Update Book successful"
            content: 
              application/json: 
                schema: 
                  $ref: "#/components/schemas/Book"
                examples: 
                  Book: 
                    value: 
                      id: 10
                      title: "WSO2 Api Managament in Action"
                      autor: "Frank E Nicolau Gonzalez"
                      price: 790
                      published: 1
                      userName: "frank\""
          404: 
            description: "Book not found"
            content: 
              application/json: 
                schema: 
                  $ref: "#/components/schemas/Error"
          500: 
            description: "Internal server error"
            content: 
              application/json: 
                schema: 
                  $ref: "#/components/schemas/Error"
        security: 
          - 
            default: []
        x-auth-type: "Application & Application User"
        x-throttling-tier: "Unlimited"
      post: 
        summary: "Adds a new book"
        requestBody: 
          content: 
            application/json: 
              schema: 
                $ref: "#/components/schemas/Book"
              examples: 
                Book: 
                  value: 
                    id: 10
                    title: "WSO2 Api Managament in Action"
                    autor: "Frank E Nicolau Gonzalez"
                    price: 790
                    published: 1
                    userName: "frank"
        responses: 
          201: 
            description: "Create Book successful"
            content: 
              application/json: 
                schema: 
                  $ref: "#/components/schemas/Book"
                examples: 
                  Book: 
                    value: 
                      id: 10
                      title: "WSO2 Api Managament in Action"
                      autor: "Frank E Nicolau Gonzalez"
                      price: 790
                      published: 1
                      userName: "frank"
          500: 
            description: "Create Book error"
            content: 
              application/json: 
                schema: 
                  $ref: "#/components/schemas/Error"
        security: 
          - 
            default: []
        x-auth-type: "Application & Application User"
        x-throttling-tier: "Unlimited"
    /books/{id}: 
      delete: 
        summary: "Delete book by id"
        description: "Delete book by id"
        parameters: 
          - 
            name: "id"
            in: "path"
            required: true
            style: "simple"
            explode: false
            schema: 
              type: "integer"
        responses: 
          200: 
            description: "Delete book ok"
            content: 
              application/json: 
                schema: 
                  $ref: "#/components/schemas/Msg"
                examples: 
                  Msg: 
                    value: 
                      text: "Removed book with id 1"
          404: 
            description: "Book not found"
            content: 
              application/json: 
                schema: 
                  $ref: "#/components/schemas/Error"
          500: 
            description: "Internal server error"
            content: 
              application/json: 
                schema: 
                  $ref: "#/components/schemas/Error"
        security: 
          - 
            default: []
        x-auth-type: "Application & Application User"
        x-throttling-tier: "Unlimited"
  components: 
    schemas: 
      Msg: 
        type: "object"
        properties: 
          text: 
            type: "string"
            example: "Removed book with id 1"
      Book: 
        required: 
          - "autor"
          - "title"
          - "userName"
        type: "object"
        properties: 
          id: 
            type: "integer"
            example: 38
          title: 
            type: "string"
            example: "WSO2 AM in Action"
          autor: 
            type: "string"
            example: "Frank E Nicolau Gonzalez"
          price: 
            type: "number"
            example: 38
          published: 
            type: "integer"
            example: 1
          userName: 
            type: "string"
            example: "frank"
          fileUri: 
            type: "string"
            example: "/frank/wso2inaction.pdf"
      Error: 
        type: "object"
        properties: 
          code: 
            type: "integer"
            example: 404
          msg: 
            type: "string"
            example: "Resource not found"
      Unauthorized: 
        type: "object"
        properties: 
          code: 
            type: "integer"
            example: 900902
          message: 
            type: "string"
            example: "Missing Credentials"
          description: 
            type: "string"
            example: "Invalid Credentials. Make sure your API invocation call has a header: ''Authorization : Bearer ACCESS_TOKEN'' or ''Authorization : Basic ACCESS_TOKEN'' or ''apikey: API_KEY"
    securitySchemes: 
      default: 
        type: "oauth2"
        flows: 
          implicit: 
            authorizationUrl: "https://test.com"
            scopes: {}
  x-wso2-auth-header: "Authorization"
  x-throttling-tier: "Unlimited"
  x-wso2-cors: 
    corsConfigurationEnabled: false
    accessControlAllowOrigins: 
      - "*"
    accessControlAllowCredentials: false
    accessControlAllowHeaders: 
      - "authorization"
      - "Access-Control-Allow-Origin"
      - "Content-Type"
      - "SOAPAction"
    accessControlAllowMethods: 
      - "GET"
      - "PUT"
      - "POST"
      - "DELETE"
      - "PATCH"
      - "OPTIONS"
  x-wso2-production-endpoints: 
    urls: 
      - "http://localhost:8080/"
    type: "http"
  x-wso2-sandbox-endpoints: 
    urls: 
      - "http://localhost:8080/"
    type: "http"
  x-wso2-basePath: "/book/1.0.0"
  x-wso2-transports: 
    - "http"
    - "https"
