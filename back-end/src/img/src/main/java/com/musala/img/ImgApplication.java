package com.musala.img;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Frank on 5/6/2020.
 */

@SpringBootApplication
public class ImgApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImgApplication.class, args);
	}

}
