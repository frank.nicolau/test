package com.musala.img.Model;

/**
 * Created by Frank on 5/6/2020.
 */
public class Msg {

    private String text;

    private Long im_id;

    public Msg(Long id, String text){
        this.text = text;
        this.im_id= id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getIm_id() {
        return im_id;
    }

    public void setIm_id(Long im_id) {
        this.im_id = im_id;
    }
}
