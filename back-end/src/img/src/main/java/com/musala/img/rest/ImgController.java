package com.musala.img.rest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import com.musala.img.Model.Error;
import com.musala.img.Model.Image;
import com.musala.img.Model.Msg;
import com.musala.img.Repository.ImageRepository;
import com.musala.img.util.Util;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Frank on 5/6/2020.
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "image")
public class ImgController {

    protected static Logger logger = Logger.getLogger(ImgController.class);

    @Autowired
    ImageRepository imageRepository;

    @PostMapping("/upload")
    public ResponseEntity<?> uplaod(@RequestParam("bookId")Long bookId, @RequestParam("imageFile") MultipartFile file)  {
        logger.info(">> Upload Image for book:"+bookId);

        try {
             Image img = new Image(file.getOriginalFilename(), file.getContentType(), compressBytes(file.getBytes()));
             img.setBook(bookId);
             imageRepository.save(img);
             Msg msg = new Msg(img.getId(),"Upload successfull");
             return new ResponseEntity<Msg>(msg, HttpStatus.OK);

        } catch (IOException io) {
            io.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, io.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e){
            e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestParam("bookId")Long bookId, @RequestParam("imageFile") MultipartFile file)  {
        logger.info(">> Update Image for book:"+bookId);

        try {

            final Optional<Image> retrievedImage = imageRepository.findByBook(bookId);
            Image img = new Image(file.getOriginalFilename(), file.getContentType(), compressBytes(file.getBytes()));
            img.setBook(bookId);
            img.setId(retrievedImage.get().getId());
            imageRepository.save(img);
            Msg msg = new Msg(img.getId(),"Update successfull");
            return new ResponseEntity<Msg>(msg, HttpStatus.OK);
        }  catch (Exception e){
            e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @GetMapping(path = { "/download" })
    public ResponseEntity<?>  download( @RequestParam (value = "imageName") String imageName) {
        logger.info(">> Download Image:"+imageName);
        try {
            final Optional<Image> retrievedImage = imageRepository.findByName(imageName);

            Image img = new Image(retrievedImage.get().getName(), retrievedImage.get().getType(),
                    decompressBytes(retrievedImage.get().getBytes()));
            return  new ResponseEntity<Image>(img, HttpStatus.OK);
        } catch (Exception e){
            e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = { "/book" })
    public ResponseEntity<?>  downloadFromBook( @RequestParam (value = "id") Long id) {
        logger.info(">> Download Image for book id:"+id);
        try {
            final Optional<Image> retrievedImage = imageRepository.findByBook(id);
            Image img = retrievedImage.get();
            img.setBytes(decompressBytes(retrievedImage.get().getBytes()));
            return  new ResponseEntity<Image>(img, HttpStatus.OK);
        } catch (Exception e){
            e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


        // compress the image bytes before storing it in the database
    public static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
             e.printStackTrace();
        }
        return outputStream.toByteArray();
    }

        // uncompress the image bytes before returning it to the angular application
    public static byte[] decompressBytes(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (DataFormatException e) {
          e.printStackTrace();
        }
        return outputStream.toByteArray();
    }
}

