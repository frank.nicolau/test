package com.musala.img.Model;

/**
 * Created by Frank on 5/6/2020.
 */
public class Error {
    private int code;
    private  String msg;


    public Error(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
