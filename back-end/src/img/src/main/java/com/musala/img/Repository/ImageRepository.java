package com.musala.img.Repository;

/**
 * Created by Frank on 5/6/2020.
 */

import java.util.Optional;
import com.musala.img.Model.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Long> {
    Optional<Image> findByName(String name);
    Optional<Image> findByBook(Long book);
}




