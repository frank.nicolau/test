package com.musala.book.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.book.Model.Book;
import com.musala.book.Model.Error;
import com.musala.book.Model.Msg;
import com.musala.book.Repository.BooKRepository;
import com.musala.book.Utils.Util;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Frank on 4/6/2020.
 */

@RestController
/*Only for localhost*/
@CrossOrigin(origins = "http://localhost:4200", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class BookController {

    protected static Logger logger = Logger.getLogger(BookController.class);

    @Autowired
    private BooKRepository repository;

    public BooKRepository getRepository() {
        return repository;
    }

    public void setRepository(BooKRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/pool")
    public String printPoolProperties() {
        return repository.getJdbcTemplate().getDataSource().toString();
    }

    @PostMapping(path = "/books")
    public ResponseEntity<?> saveBook(@RequestBody Book book ) {
        logger.info(">> Save Book MetaData:"+book);
        try {
            long id = repository.save(book);
            book.setId(id);
            return new ResponseEntity<Book>(book, HttpStatus.CREATED); // 201
        } catch (Exception e){
           e.printStackTrace();
             Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(path = "/books", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> updateBook(@RequestBody Book book) {
        logger.info(">> Update Book MetaData:"+book.getPublished());
        try {
            int id = repository.update(book);
            if(id != 0){
                return new ResponseEntity<Book>(book, HttpStatus.OK);
            }
            Error error = new Error(Util.Error_RESOURCE_NOT_FOUNT_ERROR_Code,"Could not find book");
            return new ResponseEntity<Error>(error,HttpStatus.NOT_FOUND);
        } catch (Exception e){
            e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping(path = "/books/{id}")
    public ResponseEntity<?> deleteBook( @PathVariable Long id) {
        logger.info(">> Delete Book MetaData");

        try {
            int isRemoved =  repository.delete(id);
            if (isRemoved == 0) {
                Error error = new Error(Util.Error_RESOURCE_NOT_FOUNT_ERROR_Code,"Could not find book");
                return new ResponseEntity<Error>(error,HttpStatus.NOT_FOUND);
            }

            Msg msg= new Msg("Removed book with id:"+id);
            return new ResponseEntity<Msg>(msg, HttpStatus.OK);

        } catch (Exception e){
           e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/books")
    public ResponseEntity<?> getBook( @RequestParam (value = "id") Long id ) {
        logger.info(">> Get  Book MetaData by id");
   try {
       Book book = repository.findById(id).orElseThrow(IllegalArgumentException::new);
       return new ResponseEntity<Book>(book, HttpStatus.OK);

   } catch (Exception e){
       e.printStackTrace();
       Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
       return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
     }

    }

    @GetMapping("/filterPublicBooks")
    public  ResponseEntity<?>  filterPublicBooks( @RequestParam (value = "filter") String filter,
                            @RequestParam(value = "limit") int limit,
                            @RequestParam (value = "offset") int offset ) {
        logger.info(">> Filter Public Books MetaData");
        try {

            List<Book> books= repository.getPublicBooks(filter,limit,offset);
            Map<String,Object> response = new HashMap<String,Object>();
            response.put("books",books);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e){
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/publicBooks")
    public  ResponseEntity<?>  publicBooks(
                                            @RequestParam(value = "limit") int limit,
                                            @RequestParam (value = "offset") int offset ) {
        logger.info(">> Get Public Books MetaData");
        try {

            List<Book> books= repository.getPublicBooks(limit,offset);
            Map<String,Object> response = new HashMap<String,Object>();
            response.put("books",books);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e){
            e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/countPublicBooks")
    public ResponseEntity<?> countPublicBooks( @RequestParam (value = "filter") String filter){
        logger.info(">> Count Public Books");

        try {
            int count = repository.numberOfPublicBooks(filter);
            Map<String,Object> response = new HashMap<String,Object>();
            response.put("count",count);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e){
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/totalPublicBooks")
    public ResponseEntity<?> totalPublicBooks(){
        logger.info(">> Total Public Books");

        try {
            int count = repository.numberOfPublicBooks();
            Map<String,Object> response = new HashMap<String,Object>();
            response.put("count",count);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e){
            e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @GetMapping("/filterUserBooks")
    public ResponseEntity<?> filterUserBooks(@RequestParam (value = "user") String user,
                           @RequestParam (value = "filter") String filter, @RequestParam(value = "limit") int limit,
                           @RequestParam (value = "offset") int offset ) {
        logger.info(">> Get User Books MetaData");
        try {

            List<Book> books= repository.getBooksByUserName(user,filter,limit,offset);
            Map<String,Object> response = new HashMap<String,Object>();
            response.put("books",books);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e){
            e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/userBooks")
    public ResponseEntity<?> userBooks(@RequestParam (value = "user") String user,
                                         @RequestParam(value = "limit") int limit,
                                        @RequestParam (value = "offset") int offset ) {
        logger.info(">> Get User Books MetaData");
        try {

            List<Book> books= repository.getBooksByUserName(user,limit,offset);
            Map<String,Object> response = new HashMap<String,Object>();
            response.put("books",books);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e){
            e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/countUserBooks")
    public ResponseEntity<?> countUserBooks(@RequestParam (value = "user") String user,
                                        @RequestParam (value = "filter") String filter){
        logger.info(">> Count User Books");

        try {
            int count = repository.numberOfUserBooks(user,filter);
            Map<String,Object> response = new HashMap<String,Object>();
            response.put("count",count);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e){
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/totalUserBooks")
    public ResponseEntity<?> totalUserBooks(@RequestParam (value = "user") String user
                                           ){
        logger.info(">> Total User Books");

        try {
            int count = repository.numberOfUserBooks(user);
            Map<String,Object> response = new HashMap<String,Object>();
            response.put("count",count);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e){
            e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
