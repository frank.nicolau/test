package com.musala.book.Model;

import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.sql.Blob;

/**
 * Created by Frank on 4/6/2020.
 * Book Metadata
 */
public class Book {
    private Long id;
    private String title;
    private String autor;
    private BigDecimal price;
    private int published;
    private String userName;
    private String fileUri;
    private String description;

    public Book(){}

    public Book(Long id, String title,String autor, BigDecimal price, int published, String userName,String fileUri,
                String description){
        this.id = id;
        this.title= title;
        this.autor= autor;
        this.price= price;
        this.published= published;
        this.userName= userName;
        this.fileUri=fileUri;
        this.description =description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getPublished() {
        return published;
    }

    public void setPublished(int published) {
        this.published = published;
    }

    public String getFileUri() {
        return fileUri;
    }

    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
