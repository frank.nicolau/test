package com.musala.book.Model;

/**
 * Created by Frank on 6/6/2020.
 */
public class Msg {
    String text;

    public  Msg(String text){
         this.text =text;
     }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
