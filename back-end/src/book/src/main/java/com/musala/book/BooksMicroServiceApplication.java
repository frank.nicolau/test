package com.musala.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Frank on 4/6/2020.
 */
@SpringBootApplication
public class BooksMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooksMicroServiceApplication.class, args);
	}

}
