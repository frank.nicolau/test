package com.musala.book.Utils;

import java.math.BigDecimal;

/**
 * Created by Frank on 5/6/2020.
 */

public class Util {

    public static String BOOK_ID= "id";
    public static String BOOK_TITLE= "title";
    public static String BOOK_AUTOR = "autor";
    public static String BOOK_PRICE = "price";
    public static String REPORT_USERNAME = "userName";
    public static String REPORT_PUBLIC = "isPublic";

    public static  int Error_RESOURCE_NOT_FOUNT_ERROR_Code = 404;
    public static  int Error_INTERNAL_SERVER_ERROR_Code = 500;

}
