package com.musala.book.Repository;

import com.musala.book.Model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Created by Frank on 4/6/2020.
 */
@Repository
public class BooKRepository {

    @Autowired
  private JdbcTemplate jdbcTemplate;

  public BooKRepository(){}

  public JdbcTemplate getJdbcTemplate() {
   return jdbcTemplate;
  }

  public long  save(Book book) throws Exception {
      String sql = "insert into books (title, price,autor,published,userName,fileUri,description) values(?,?,?,?,?,?,?)";
      KeyHolder keyHolder = new GeneratedKeyHolder();
      jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement pst =
                                con.prepareStatement(sql, new String[]{"id"});
                        pst.setString(1, book.getTitle());
                        pst.setBigDecimal(2, book.getPrice());
                        pst.setString(3, book.getAutor());
                        pst.setInt(4, book.getPublished());
                        pst.setString(5, book.getUserName());
                        pst.setString(6, book.getFileUri());
                        pst.setString(7, book.getDescription());
                        return pst;
                    }
                },
                keyHolder);
        final Number id = keyHolder.getKey();

        return id.longValue();


    }



  public int update(Book book)throws Exception{
      return jdbcTemplate.update(
              "update books set title = ?, price = ?, autor = ?, published = ?, fileUri = ?,  description = ? where id = ?",
             book.getTitle(), book.getPrice(), book.getAutor(),
             book.getPublished(),book.getFileUri(), book.getDescription(),book.getId());
  }

  public int delete(Long id) throws Exception{
      return jdbcTemplate.update(
              "delete FROM books where id = ?",
              id);
  }



   public List<Book> getPublicBooks(String filter, Integer  limit, Integer offset)throws Exception{
       return jdbcTemplate.query(
               "select * from books WHERE published = ? AND title LIKE ? OR autor LIKE ? ORDER BY title ASC LIMIT ? OFFSET ?",
               new Object[]{1,"%"+filter+"%","%"+filter+"%", limit, offset},
               (rs, rowNum) ->
                       new Book (
                               rs.getLong("id"),
                               rs.getString("title"),
                               rs.getString("autor"),
                               rs.getBigDecimal("price"),
                               rs.getInt("published"),
                               rs.getString("userName"),
                               rs.getString("fileUri"),
                               rs.getString("description")
                       )
       );
   }

    public List<Book> getPublicBooks(Integer  limit, Integer offset)throws Exception{
        return jdbcTemplate.query(
                "select * from books WHERE published = ? ORDER BY title ASC LIMIT ? OFFSET ?",
                new Object[]{1, limit, offset},
                (rs, rowNum) ->
                        new Book (
                                rs.getLong("id"),
                                rs.getString("title"),
                                rs.getString("autor"),
                                rs.getBigDecimal("price"),
                                rs.getInt("published"),
                                rs.getString("userName"),
                                rs.getString("fileUri"),
                                rs.getString("description")
                        )
        );
    }



   public List<Book> getBooksByUserName(String userName, String filter, Integer  limit, Integer offset)throws Exception{

       return jdbcTemplate.query (
               "select * from books WHERE userName = ? AND title LIKE ? OR autor LIKE ? ORDER BY title ASC LIMIT ? OFFSET ?",
               new Object[]{userName,"%"+filter+"%","%"+filter+"%", limit, offset},
               (rs, rowNum) ->
                       new Book (
                               rs.getLong("id"),
                               rs.getString("title"),
                               rs.getString("autor"),
                               rs.getBigDecimal("price"),
                               rs.getInt("published"),
                               rs.getString("userName"),
                               rs.getString("fileUri"),
                               rs.getString("description")
                       )
       );
   }

    public List<Book> getBooksByUserName(String userName, Integer  limit, Integer offset)throws Exception {

        return jdbcTemplate.query(
                "select * from books WHERE userName = ?  ORDER BY title ASC LIMIT ? OFFSET ?",
                new Object[]{userName, limit, offset},
                (rs, rowNum) ->
                        new Book(
                                rs.getLong("id"),
                                rs.getString("title"),
                                rs.getString("autor"),
                                rs.getBigDecimal("price"),
                                rs.getInt("published"),
                                rs.getString("userName"),
                                rs.getString("fileUri"),
                                rs.getString("description")
                        )
        );
    }


    public Optional<Book> findById(Long id) throws Exception {
        return jdbcTemplate.queryForObject(
                "select * from books where id = ?",
                new Object[]{id},
                (rs, rowNum) ->
                        Optional.of(new Book(
                                rs.getLong("id"),
                                rs.getString("title"),
                                rs.getString("autor"),
                                rs.getBigDecimal("price"),
                                rs.getInt("published"),
                                rs.getString("userName"),
                                rs.getString("fileUri"),
                                rs.getString("description")
                        ))
        );
    }


    public int publish(Long id)throws Exception{
        return jdbcTemplate.update(
                "update books set isPublic = ? where id = ?",
                 true, id);
    }

    public int unpublish(Long id)throws Exception{
        return jdbcTemplate.update(
                "update books set isPublic = ?  where id = ?",
                false, id);
    }

    public int numberOfPublicBooks(String filter){
        String sql = "select count(*) from books WHERE published = ? AND title LIKE ? OR autor LIKE ?";
        return getJdbcTemplate().queryForObject(
                sql, new Object[] { 1, "%"+filter+"%","%"+filter+"%"}, Integer.class);
    }

    public int numberOfUserBooks(String user,String filter){
        String sql = "select count(*) from books WHERE userName = ? AND title LIKE ? OR autor LIKE ?";
        return getJdbcTemplate().queryForObject(
                sql, new Object[] { user,"%"+filter+"%","%"+filter+"%" }, Integer.class);
    }

    public int numberOfPublicBooks()
    {
        String sql = "select count(*) from books WHERE published = ?";
        return getJdbcTemplate().queryForObject(
                sql, new Object[] { 1}, Integer.class);
    }

    public int numberOfUserBooks(String user){
        String sql = "select count(*) from books WHERE userName = ?";
        return getJdbcTemplate().queryForObject(
                sql, new Object[] { user }, Integer.class);
    }

}
