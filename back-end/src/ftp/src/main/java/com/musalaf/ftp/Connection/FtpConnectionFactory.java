package com.musalaf.ftp.Connection;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.IOException;
import org.apache.log4j.Logger;

/**
 * Created by Frank on 6/6/2020.
 */
public class FtpConnectionFactory {
    private String server;
    private int port;
    private String user;
    private String password;
    protected static Logger logger = Logger.getLogger(FtpConnectionFactory.class);


    public FtpConnectionFactory(){}

    public FtpConnectionFactory(String server, int port, String user, String password ){
        this.server = server;
        this.port=port;
        this.user=user;
        this.password=password;
    }

    public FTPClient getConnection() throws IOException {
        FTPClient ftp = new FTPClient();

        ftp.connect(server, port);

       if(!ftp.login(user, password))
       {
           ftp.logout();
           logger.info("Login Error");
       }

        int reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            logger.info("Connection Error");
        }

        return ftp;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
