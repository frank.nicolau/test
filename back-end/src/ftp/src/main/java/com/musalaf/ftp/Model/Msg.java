package com.musalaf.ftp.Model;

/**
 * Created by Frank on 20/6/2020.
 */
public class Msg {

    private String text;
    private String fileUri;
    private long size;

    public Msg(String fileUri, String text, long size){
        this.text = text;
        this.fileUri = fileUri;
        this.size =size;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFileUri() {
        return fileUri;
    }

    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

}
