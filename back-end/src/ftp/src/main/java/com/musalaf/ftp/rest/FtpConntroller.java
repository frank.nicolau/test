package com.musalaf.ftp.rest;

import com.musalaf.ftp.Connection.FtpConnectionFactory;
import com.musalaf.ftp.Model.Msg;
import com.musalaf.ftp.util.Util;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.musalaf.ftp.Model.Error;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Frank on 6/6/2020.
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "ftp")
public class FtpConntroller {

    protected static Logger logger = Logger.getLogger(FtpConntroller.class);

    @Autowired
    FtpConnectionFactory factory;

    @PostMapping("/upload")
    public ResponseEntity<?> uplaod( @RequestParam("file") MultipartFile file)  {
        String fileName = file.getOriginalFilename();
        logger.info(">> Upload file:"+fileName);

        try {
            String timeStamp = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(Calendar.getInstance().getTime());
            String fileDir = timeStamp.split(" ")[0];
            FTPClient ftp = factory.getConnection();
            //enter passive mode
            ftp.enterLocalPassiveMode();
            ftp.setFileType(FTP.BINARY_FILE_TYPE);

            ftp.makeDirectory(fileDir);
            ftp.changeWorkingDirectory(fileDir);
            String fileUri = fileDir+"-"+fileName;
            ftp.storeFile(fileUri,file.getInputStream());
            Msg msg = new Msg(fileUri,"Upload successfull",file.getSize());
            return  new ResponseEntity<Msg>(msg, HttpStatus.OK);

        } catch (IOException io){
            io.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, io.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e){
            e.printStackTrace();
            Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
            return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/download")
    public void download(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam("fileUri") String fileUri) {
        logger.info(">> download file:"+fileUri);

          String[] array = fileUri.split("-");
         if (array.length > 3 ) {
              String fileDir = array[0]+"-"+array[1]+"-"+array[2];
              String fileName = fileUri.substring(11);

             try {
                 FTPClient ftp = factory.getConnection();
                 ftp.enterLocalPassiveMode();
                 ftp.setFileType(FTP.BINARY_FILE_TYPE);
                 ftp.changeWorkingDirectory(fileDir);
                 response.setContentType("application/pdf");
                 response.setHeader("Content-Disposition",
                         "attachment; filename="+fileName+".pdf");
                 OutputStream output = response.getOutputStream();
                 ftp.retrieveFile(fileUri, output);
                 //close and flush output stream
                 output.close();
                 output.flush();

             } catch (IOException io){
                 io.printStackTrace();
                 sendError(io.getMessage(),response,Util.Error_INTERNAL_SERVER_ERROR_Code);
             } catch (Exception e){
                 e.printStackTrace();
                 sendError(e.getMessage(),response,Util.Error_INTERNAL_SERVER_ERROR_Code);
             }
         } else {
             sendError("FileUri is not valid",response,Util.Error_RESOURCE_NOT_FOUNT_ERROR_Code);
         }
    }

    @DeleteMapping(path = "/delete/{fileUri}")
    public ResponseEntity<?> deleteFile( @PathVariable String fileUri) {
        logger.info(">> Delete File:"+fileUri);

        String[] array = fileUri.split("-");
        if (array.length > 3 ) {
            String fileDir = array[0]+"-"+array[1]+"-"+array[2];
            String fileName = fileUri.substring(11);
            try {
                FTPClient ftp = factory.getConnection();
                ftp.enterLocalPassiveMode();
                ftp.setFileType(FTP.BINARY_FILE_TYPE);
                ftp.changeWorkingDirectory(fileDir);
                if( ftp.deleteFile(fileUri)) {
                    Msg msg = new Msg(fileUri, "Delete file successfull", 0);
                    return new ResponseEntity<Msg>(msg, HttpStatus.OK);
                }
                Error error = new Error(Util.Error_RESOURCE_NOT_FOUNT_ERROR_Code,"FileUri is not valid");
                return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);

            } catch (IOException io){
                io.printStackTrace();
                Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, io.getMessage());
                return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);

            } catch (Exception e){
                e.printStackTrace();
                Error error = new Error(Util.Error_INTERNAL_SERVER_ERROR_Code, e.getMessage());
                return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } else {
            Error error = new Error(Util.Error_RESOURCE_NOT_FOUNT_ERROR_Code,"FileUri is not valid");
            return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
        }
    }

    public void sendError(String errorMsg, HttpServletResponse response, int code) {

        response.setContentType("application/json");
        response.setStatus(code);
        String jsonError= "{ \"sc\":500," +
                "\"msg\":"+"\""+errorMsg+"\""+"}";
        try {
            response.setContentLength(jsonError.length());
            response.getWriter().write(jsonError);
            response.getWriter().flush();

        } catch (IOException ie){
            ie.printStackTrace();
        }
    }
}
