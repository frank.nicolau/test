export const environment = {
  production: true,
  navitems:[
    {
        icon: "fa fa-home",
        navBarText:"Private Books",
        text:"Private Books",
        rute:"private",
        needSearch: 1,
        needAuth:1
    },
    {
        icon: "fa fa-bank",
        navBarText:"Public Books",
        text:"Public Books",
        rute:"welcome",
        needSearch: 1,
        needAuth:0
    },
    {
      icon: "fa fa-user-circle-o",
      navBarText:"User Profile",
      text:"User Profile",
      rute:"profile",
      needSearch: 0,
      needAuth:1
    },
    {
        icon: "fa fa-address-book",
        navBarText:"Contact us",
        text:"Contact us",
        rute:"contact",
        needSearch: 0,
        needAuth:0
    }
 ], 

 bookApiUrl: 'https://localhost:8244/book/1.0.0',
 imgApiUrl: 'https://localhost:8244/image/1.0.0',
 ftpApiUrl:'https://localhost:8244/ftp/1.0.0',
 publicBooksApi:'https://localhost:8244/books/1.0.0',
 publicBooksApiKey:'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ik5UZG1aak00WkRrM05qWTBZemM1TW1abU9EZ3dNVEUzTVdZd05ERTVNV1JsWkRnNE56YzRaQT09In0.eyJhdWQiOiJodHRwOlwvXC9vcmcud3NvMi5hcGltZ3RcL2dhdGV3YXkiLCJzdWIiOiJhZG1pbkBjYXJib24uc3VwZXIiLCJhcHBsaWNhdGlvbiI6eyJvd25lciI6ImFkbWluIiwidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsInRpZXIiOiJVbmxpbWl0ZWQiLCJuYW1lIjoiQm9va1N0b3JlIiwiaWQiOjEsInV1aWQiOm51bGx9LCJzY29wZSI6ImFtX2FwcGxpY2F0aW9uX3Njb3BlIGRlZmF1bHQiLCJpc3MiOiJodHRwczpcL1wvbG9jYWxob3N0Ojk0NDRcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJCcm9uemUiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH0sIkdvbGQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH0sIlVubGltaXRlZCI6eyJ0aWVyUXVvdGFUeXBlIjoicmVxdWVzdENvdW50Iiwic3RvcE9uUXVvdGFSZWFjaCI6dHJ1ZSwic3Bpa2VBcnJlc3RMaW1pdCI6MCwic3Bpa2VBcnJlc3RVbml0IjpudWxsfX0sImtleXR5cGUiOiJQUk9EVUNUSU9OIiwic3Vic2NyaWJlZEFQSXMiOlt7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJjYXJib24uc3VwZXIiLCJuYW1lIjoiSW1hZ2UiLCJjb250ZXh0IjoiXC9pbWFnZVwvMS4wLjAiLCJwdWJsaXNoZXIiOiJhZG1pbiIsInZlcnNpb24iOiIxLjAuMCIsInN1YnNjcmlwdGlvblRpZXIiOiJHb2xkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoiY2FyYm9uLnN1cGVyIiwibmFtZSI6IlB1YmxpY0Jvb2tzIiwiY29udGV4dCI6IlwvYm9va3NcLzEuMC4wIiwicHVibGlzaGVyIjoiYWRtaW4iLCJ2ZXJzaW9uIjoiMS4wLjAiLCJzdWJzY3JpcHRpb25UaWVyIjoiQnJvbnplIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoiY2FyYm9uLnN1cGVyIiwibmFtZSI6IlByaXZhdGVCb29rcyIsImNvbnRleHQiOiJcL3ByaXZhdGVcLzEuMC4wIiwicHVibGlzaGVyIjoiYWRtaW4iLCJ2ZXJzaW9uIjoiMS4wLjAiLCJzdWJzY3JpcHRpb25UaWVyIjoiR29sZCJ9LHsic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImNhcmJvbi5zdXBlciIsIm5hbWUiOiJCb29rIiwiY29udGV4dCI6IlwvYm9va1wvMS4wLjAiLCJwdWJsaXNoZXIiOiJhZG1pbiIsInZlcnNpb24iOiIxLjAuMCIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sImNvbnN1bWVyS2V5IjoiVFRnR3llX01mNTVHMHVFRUV4OTFqQmIzdXBNYSIsImV4cCI6Mzc0MDEwODY3NSwiaWF0IjoxNTkyNjI1MDI4LCJqdGkiOiIwODEzMGRhZS01NTAyLTQ3ODgtODQ2ZS0yOTViZjM5NDMzMTYifQ.uaenQLjKtjHfOgLi1OrHLohVkPlelUBGscWZ7-mHEP8PJ-GwkCncAvwJ2uPRK1QwUAs5SSGcoDG0sg5WQNGGVqFShUgm3OAyoVxsmW9I51fRGM9atfMoz_t9DdizigFbyulTMWEaWIH0dygjWZwH3tIg9srsL4IILmZ-aFjqw_RcP5HvxtAxh5sp0S8Pa2bKjYVXEB3Q9whcE7I1BzL7cD_bEUStTmkSwex4p6ua5IgR8ExC-zGBiQQgG6aq5XmFfbD3mLMxJnbYB15W4Cu-n4zNVr0vNxd2dAOOpUz85P7cGfSLOKGjbbaq3KoXUOTpkaYKF0YqW0fRkTqT1wlbZA',
 privateBooksApiUrl:'https://localhost:8244/private/1.0.0',
 userPortalUrl:'https://localhost:9443/user-portal',
 oauth2Configuration: {
    tokenUrl: 'https://localhost:9444/oauth2/token',
    clientId: 'TTgGye_Mf55G0uEEEx91jBb3upMa',
    clientSecret: 'C8lRvijJmB7N_jVFapxrDHFXsGca' 
 },
 openIdConfiguration: {
  stsServer: 'https://localhost:9443/oauth2/oidcdiscovery',
  redirectUrl: 'http://localhost:4200/private',
  clientId: 'joJ0fWs1iZSVU7fL76M63YEmXfoa',
  responseType: 'code',
  scope: 'openid profile email',
  triggerAuthorizationResultEvent: true,
  postLogoutRedirectUri: window.location.origin + '/welcome',
  startCheckSession: false,
  silentRenew: false,
  silentRenewUrl: 'http://localhost:4200/assets/silent-renew.html',
  postLoginRoute: '/private',
  forbiddenRoute: '/forbidden',
  unauthorizedRoute: '/unauthorized',
  logLevel: 0,
  historyCleanupOff: true
}

};
