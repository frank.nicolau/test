export interface Navitem {
    icon:string;
    navBarText:string;
    text:string;
    rute:string;
    needSearch:number;
    needAuth:number;

}
