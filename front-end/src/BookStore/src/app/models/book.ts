import { Image } from './image';

export class Book {
    title: string;
    autor: string;
    published: number;
    fileUri?:string;
    userName:string;
    description:string;
    id?:number;
    img?:Image;
    price:number;
}


