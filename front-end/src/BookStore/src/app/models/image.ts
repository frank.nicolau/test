export interface Image {
    name?: string;
    type?: string;
    id?:number;
    bytes:string;
}