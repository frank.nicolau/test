
export interface User {
    sub: string;
    family_name?:string;
    given_name?:string;
    email?:string;
}
