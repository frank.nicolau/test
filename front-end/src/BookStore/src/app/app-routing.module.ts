import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { ContactComponent } from './components/contact/contact.component';
import { PrivateBookComponent } from './components/private-book/private-book.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AddComponent } from './components/forms/add/add.component';
import { EditComponent } from './components/forms/edit/edit.component';
import { DetailsViewComponent } from './components/details/details-view/details-view.component';
import { ForbiddenComponent } from './components/forbidden/forbidden.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'welcome'},
  {path: 'welcome', component: WelcomeComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'private', component: PrivateBookComponent, canActivate: [ AuthGuard]},  //protect private rute
  {path: 'profile', component: ProfileComponent,canActivate: [ AuthGuard]},
  {path: 'add', component: AddComponent, canActivate: [ AuthGuard]},
  {path: 'edit/:id', component: EditComponent, canActivate: [ AuthGuard]},
  {path: 'details/:id', component: DetailsViewComponent},
  {path: 'forbidden',component: ForbiddenComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
