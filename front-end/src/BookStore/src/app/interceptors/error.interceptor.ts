import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { MsgService } from '../services/msg.service';
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/internal/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private msg:MsgService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError(error => {
        let errorMessage = '';
        if (error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Client-side error: ${error.error.message}`;
        } else {
          // backend error
          errorMessage = `Server-side error: ${error.status} ${error.message}`;
        }
        
        this.msg.error(errorMessage,false,true);
        return throwError(errorMessage);
      })
    );
  }
}
