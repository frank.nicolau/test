import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({providedIn: 'root'})
export class AuthInterceptor implements HttpInterceptor {


  constructor(private auth:AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
   
    if(this.auth.isAuth() == true) {
      const clone = request.clone({
         headers: request.headers.set('Authorization', 'Bearer ' + this.auth.getAccessToken())
    });
       return next.handle(clone);
    } 

     return next.handle(request);
  }
}
