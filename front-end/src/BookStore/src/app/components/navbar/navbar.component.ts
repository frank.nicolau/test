import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Navitem } from 'src/app/models/navitem';
import { environment } from 'src/environments/environment';
import { PaginationService } from 'src/app/services/pagination.service';
import { Subscription, fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { SearchService } from 'src/app/services/search.service';
import { AuthService } from 'src/app/services/auth.service';
import { NavbarService } from 'src/app/services/navbar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  title:string;
  navigation:string;
  navitems:Array<Navitem>;
  pages:Array<number>;
  needSearch: number;
  private pageSubscription: Subscription;

  constructor(private paginator: PaginationService, private navbarService:NavbarService,
    private searchService: SearchService, private authService:AuthService) { 
    this.navitems = environment.navitems;
    this.pages=[1];
    this.needSearch = 1;

  }
  
  search() {
    if(this.needSearch == 1)
       return true;
    return false;
  }

  ngOnInit(): void {
     this.navbarService.navItemChange().subscribe(item => {
     this.title = item.navBarText;
     this.needSearch = item.needSearch;
     this.navigation = item.rute;
    });       
     
      
    this.pageSubscription = this.paginator.totalPageChange().subscribe(total =>{
        let page=1;
        this.pages =[];
        for(var i = 0; i < total; i++)
        {
           this.pages[i] = page;
           ++page;
          }
       })
     
       const searchInput = document.getElementById('search');
       const typeahead = fromEvent(searchInput, 'input').pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2 || text.length == 0),
        debounceTime(1000),
        distinctUntilChanged()
      );
      typeahead.subscribe(data => {
         this.searchService.search(data);
       });    

  }

  isAuth() {
    return this.authService.isAuth();
  }

  login(){
    this.authService.login();
  }

  logout() {
    this.authService.logout();
  }

  ngOnDestroy(){
    this.pageSubscription.unsubscribe();
   }

   pageChange(page:any){
      this.paginator.updateOffset(page);
   }

}
