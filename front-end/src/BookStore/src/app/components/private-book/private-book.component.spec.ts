import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateBookComponent } from './private-book.component';

describe('PrivateBookComponent', () => {
  let component: PrivateBookComponent;
  let fixture: ComponentFixture<PrivateBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
