import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/models/book';
import { SearchService } from 'src/app/services/search.service';
import { BooksService } from 'src/app/services/books.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { Subscription } from 'rxjs';
import { ImgService } from 'src/app/services/img.service';
import { AuthService } from 'src/app/services/auth.service';
import { MsgService } from 'src/app/services/msg.service';
import { Router } from '@angular/router';
import { FtpService } from 'src/app/services/ftp.service';

@Component({
  selector: 'app-private-book',
  templateUrl: './private-book.component.html',
  styleUrls: ['./private-book.component.css']
})
export class PrivateBookComponent implements OnInit {

 rows: Array<Book[]> = []; 
 offset:number;
 limit:number;
 filter:string;
 itemForRow:number;
 private offsetSubscription: Subscription;
 private searchSubscription: Subscription;
 result:any ='';


constructor(private bookService:BooksService, private paginatorService: PaginationService,
   private searchService: SearchService, private imageService:ImgService, private ftpService: FtpService ,
   private authService:AuthService, private msgService:MsgService, private router:Router) {
    this.offset = 0;
    this.limit = paginatorService.getlimit();
    this.filter=searchService.getFilterValue();
    this.itemForRow = 3;
  }

  ngOnInit(): void { 

    this.offsetSubscription = this.paginatorService.offsetChange().subscribe(offset =>{
      this.bookService.privateBooks(this.authService.userName(),this.filter, this.limit, offset).subscribe( (data:any)=> {
        this.rows = this.groupArray<Book>(data,this.itemForRow);
        this.offset = offset;
       });
     });

    this.searchSubscription =  this.searchService.valueChange().subscribe(filter =>{
      this.updateBooks(filter, 0);

     });

    this.updateBooks(this.filter, this.offset); 
  }

  ngOnDestroy() {
    this.offsetSubscription.unsubscribe();
    this.searchSubscription.unsubscribe();
   }

   deleteBook(book:Book) {

    if(window.confirm('Are you sure you want to delete the book ?')){
      this.bookService.deleteBook(book.id).subscribe(data => {
        this.msgService.success("The book was successfully delete...",false,true); 
        this.updateBooks(this.filter,this.offset);
          
          if(this.isStore(book)) {
            this.ftpService.deleteFile(book.fileUri).subscribe(data=>{
              this.msgService.success("The book was successfully from ftp...",false,true); 
            })
          }
      });
    }   
   }

   editBook(book:Book) {
     this.router.navigate(['edit',book.id]);
   }

  public updateBooks(filter:string, offset: number){
    this.bookService.countPrivateBooks(this.authService.userName(),filter).subscribe( (count:any) => {
      let total = count.value;
      this.rows = [];
      if(total == 0){
         this.msgService.success("There are no books to show you",false,true);
      } else {
        this.paginatorService.updateTotal(total);
        this.bookService.privateBooks(this.authService.userName(), filter, this.limit, offset).subscribe( (data:any)=> {
        this.rows = this.groupArray<Book>(data,this.itemForRow);
        this.filter=filter;
        this.offset=offset;
       });
      }

     });
  }

   groupArray<T>(data: Array<T>, n: number): Array<T[]> {
    let group = new Array<T[]>();
​
    for (let i = 0, j = 0; i < data.length; i++) {
        if (i >= n && i % n === 0)
            j++;
        group[j] = group[j] || [];
        group[j].push(data[i])
    }
​
    return group;
}

getImgSrc(book:Book){
  if(book.img.bytes == null) return "./assets/img/default.jpg";
  return "data:image/jpeg;base64,"+book.img.bytes;
}

publish(book:Book) {
  if(book.published == 1)
      book.published = 0;
  else
      book.published = 1;

    this.bookService.updateBook(book).subscribe(data => {
      this.msgService.success("Book update succesfull",false,true);
   });

}

preview(files, book: Book) {
    
  if(files.length === 0)
    return;
   var mimeType = files[0].type;
   let file = files[0];
   var pattern=/image-*/;
   if(!file.type.match(pattern)) {
      alert("Invalid format");
      return;
   }
   var reader = new FileReader();
   reader.onload = (_event)=> {
      this.result = reader.result;//reader.result;
       if(book.img.bytes == null) {
          this.imageService.upload(file, book.id).subscribe( data => {
            book.img.bytes = this.result.split(',')[1];  
          });
       }
       else {

       }
   }

   reader.readAsDataURL(file);
}

uploadFile(files, book: Book){
  if(files.length === 0)
  return;
 var mimeType = files[0].type;
 let file = files[0];

 if(file.type != 'application/pdf') {
  alert(" Invalid format");
  return;
 }

 this.ftpService.upload(file).subscribe((data:any) =>{
    book.fileUri = data.fileUri;
    this.bookService.updateBook(book).subscribe(data =>{
      this.msgService.success("Book update succesfull",false,true);
    });
 })

}

 isStore(book:Book) {
     if(book.fileUri === "empty" || book.fileUri == null) return false;
     return true; 
 }


 downloand(book:Book) {

   if(this.isStore(book))
    this.ftpService.download(book.fileUri).subscribe( resp => {
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(resp);
      link.download = book.fileUri.slice(11);
      console.log(resp);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    });
 }

}
