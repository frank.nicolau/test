import { Component, OnInit } from '@angular/core';
import { Navitem } from 'src/app/models/navitem';
import { environment } from 'src/environments/environment';
import { SidebarService } from 'src/app/services/sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  items:Array<Navitem>;
  constructor(private sidebarService:SidebarService) { 
    //Subcribe to sideBar service
    this.sidebarService.itemsChange().subscribe(items =>{
       this.items=items;
    });

  }

  ngOnInit(): void {
  }

}
