import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MsgService } from 'src/app/services/msg.service';

@Component({
  selector: 'app-msg',
  templateUrl: './msg.component.html',
  styleUrls: ['./msg.component.css']
})
export class MsgComponent implements OnInit {

  private subscription: Subscription;
  message:any;
  constructor(private messageService: MsgService) { 
    
  }

  ngOnInit() {
    this.subscription = this.messageService.getMessage().subscribe( message=>{
      this.message = message;
    })
  }

  ngOnDestroy(){
      this.subscription.unsubscribe();
  }

}
