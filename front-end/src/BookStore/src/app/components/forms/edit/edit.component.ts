import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/services/msg.service';
import { BooksService } from 'src/app/services/books.service';
import { AuthService } from 'src/app/services/auth.service';
import { Book } from 'src/app/models/book';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  book:Book = {
    id:0,
    title: "",
    autor: "",
    published: 0,
    fileUri:"empty",
    userName:"",
    description:"",
    img:{
      name: "",
      type: "",
      bytes: ""
    },
    price:0.0
  };
  constructor(private bookService: BooksService, private authservice: AuthService,
    private activeRoute:ActivatedRoute,private router: Router) { 
      this.book.id = activeRoute.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.bookService.getBook(this.book.id).subscribe((data:Book) => {
      this.book=data;
    })
  }

  editBook() {
    this.book.userName = this.authservice.userName();
    this.bookService.updateBook(this.book).subscribe(data=> {
      this.router.navigate(['private']);
    });
  }
}
