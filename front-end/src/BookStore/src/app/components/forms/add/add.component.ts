import { Component, OnInit, ViewChild } from '@angular/core';
import { BooksService } from 'src/app/services/books.service';
import { AuthService } from 'src/app/services/auth.service';

import { Book } from 'src/app/models/book';
import { MsgService } from 'src/app/services/msg.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  book:Book = {
    title: "",
    autor: "",
    published: 0,
    fileUri:"empty",
    userName:"",
    description:"",
    img:{
      name: "",
      type: "",
      bytes: ""
    },
    price:0.0
  };
  constructor(private bookService: BooksService, private authservice: AuthService,
    private msg: MsgService) { }

  ngOnInit(): void {

  }

  saveBook(bookForm: NgForm){
    this.book.userName = this.authservice.userName();
    this.bookService.addBook(this.book).subscribe(data =>{
      this.msg.success("Append book sucessfull.",false,true);
        bookForm.reset();
    })
  }


}
