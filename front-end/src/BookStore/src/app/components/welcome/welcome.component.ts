import { Component, OnInit } from '@angular/core';
import { BooksService } from 'src/app/services/books.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { Book } from 'src/app/models/book';
import { Subscription } from 'rxjs';
import { SearchService } from 'src/app/services/search.service';
import { MsgService } from 'src/app/services/msg.service';
import { FtpService } from 'src/app/services/ftp.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

 rows: Array<Book[]> = []; 
 offset:number;
 limit:number;
 filter:string;
 itemForRow:number;
 private offsetSubscription: Subscription;
 private searchSubscription: Subscription;


  constructor(private bookService:BooksService, private paginatorService: PaginationService,
   private searchService: SearchService,private ftpService: FtpService, private msgService:MsgService) {
    this.offset = 0;
    this.limit = paginatorService.getlimit();
    this.filter=searchService.getFilterValue();
    this.itemForRow = 3;

   }

  ngOnInit(): void { 

    this.offsetSubscription = this.paginatorService.offsetChange().subscribe(offset =>{
      this.bookService.publicBooks(this.filter, this.limit, offset).subscribe( (data:any)=> {
        this.rows = this.groupArray<Book>(data, this.itemForRow);
        this.offset = offset;
       });
     });

    this.searchSubscription =  this.searchService.valueChange().subscribe(filter =>{
      this.updateBooks(filter, 0);

     });

    this.updateBooks(this.filter, this.offset); 
  }

  ngOnDestroy() {
    this.offsetSubscription.unsubscribe();
    this.searchSubscription.unsubscribe();
   }

  public updateBooks(filter:string, offset: number){
    this.bookService.countPublicBooks(filter).subscribe( (count:any) => {
      let total = count.value;
      this.rows = [];
      if(total == 0){
        this.msgService.success("There are no books to show you",false,true);
      } else {
        this.paginatorService.updateTotal(total);
        this.bookService.publicBooks(filter, this.limit, offset).subscribe( (data:any)=> {
        this.rows = this.groupArray<Book>(data,this.itemForRow);
        this.filter=filter;
        this.offset=offset;
       });
      }

     });
  }

   groupArray<T>(data: Array<T>, n: number): Array<T[]> {
    let group = new Array<T[]>();
​
    for (let i = 0, j = 0; i < data.length; i++) {
        if (i >= n && i % n === 0)
            j++;
        group[j] = group[j] || [];
        group[j].push(data[i])
    }
​
    return group;
}

hasImg(book:Book){
    if(book.img.bytes == "" || book.img.bytes == null) return false;
    return true;
}


isStore(book:Book) {
  if(book.fileUri === "empty" || book.fileUri == null) return false;
  return true; 
}


downloand(book:Book) {

if(this.isStore(book))
 this.ftpService.download(book.fileUri).subscribe( resp => {
   var link = document.createElement('a');
   link.href = window.URL.createObjectURL(resp);
   link.download = book.fileUri.slice(11);
   console.log(resp);
   document.body.appendChild(link);
   link.click();
   document.body.removeChild(link);
 });
}
}
