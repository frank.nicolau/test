import { Component, OnInit } from '@angular/core';
import { BooksService } from 'src/app/services/books.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from 'src/app/models/book';

@Component({
  selector: 'app-details-view',
  templateUrl: './details-view.component.html',
  styleUrls: ['./details-view.component.css']
})
export class DetailsViewComponent implements OnInit {

  book:Book = {
    id:0,
    title: "",
    autor: "",
    published: 0,
    fileUri:"",
    userName:"",
    description:"",
    img:{
      name: "",
      type: "",
      bytes: ""
    },
    price:0.0
  };

  constructor(private bookService: BooksService,
    private activeRoute:ActivatedRoute,private router: Router) { 
      this.book.id = activeRoute.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.bookService.getBook(this.book.id).subscribe((data:Book) => {
      this.book = data;
    })
  }

}
