import { Component, OnInit, Input } from '@angular/core';
import { Book } from 'src/app/models/book';

@Component({
  selector: 'app-details-book',
  templateUrl: './details-book.component.html',
  styleUrls: ['./details-book.component.css']
})
export class DetailsBookComponent implements OnInit {

  @Input()
  b:Book = {
    id:0,
    title: "",
    autor: "",
    published: 0,
    fileUri:"",
    userName:"",
    description:"",
    img:{
      name: "",
      type: "",
      bytes: ""
    },
    price:0.0
  };

  constructor() { }
  ngOnInit(): void {}

  getImgSrc() {
    // if(this.b.img.bytes == null || this.b.img.bytes == "") 
    return "./assets/img/default.jpg";
    // return "data:image/jpeg;base64,"+this.b.img.bytes;
  }
  

}
