import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NavbarService } from './navbar.service';
import { SidebarService } from './sidebar.service';
import { Navitem } from '../models/navitem';
import { filter } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {


  private navitems:Array<Navitem>;

  constructor(private router: Router, private navbarService:NavbarService,
    private sidebarService:SidebarService) { 

      this.navitems = environment.navitems;
      this.updateSideBar(false);
    
      router.events.pipe(
        filter(event => event instanceof NavigationEnd)
       ).subscribe(event => {
        
        this.navitems.forEach((item)=> {      
            if(this.router.url.search(item.rute) !== -1)
            {
              navbarService.setNavItem(item); // Notify navbar
            } 
          })
        });
  }

   updateSideBar(auth:boolean) {
     var items= Array<Navitem>();
    
    this.navitems.forEach((item)=> {   
      if(item.needAuth == 0)   
         items.push(item)
      else {
          if(auth == true)
            items.push(item);
      }

    });
    this.sidebarService.setItems(items); 
  }
}