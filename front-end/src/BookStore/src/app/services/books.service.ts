import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Book } from '../models/book';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private bookUrl:string = environment.bookApiUrl+ '/books';
  private privateBooksUrl:string = environment.privateBooksApiUrl+'/privateBooks';
  private countPrivateBooksUrl:string = environment.privateBooksApiUrl+'/countPrivateBooks';
  private publicBooksUrl:string  = environment.publicBooksApi+'/publicBooks';
  private detailsBookUrl:string = environment.publicBooksApi+'/details';
  private countPublicBooksUrl:string = environment.publicBooksApi+'/countPublicBooks';
  private publicbooksAPIkey:string = environment.publicBooksApiKey;

  constructor(private http: HttpClient, private auth:AuthService) { 
  }

  countPublicBooks(filter:string) {
    let strParams ='filter='+filter;
    const httpOptions = {
      headers: new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + this.publicbooksAPIkey),
      params: new HttpParams({fromString: strParams})
    };
    return this.http.get(this.countPublicBooksUrl,httpOptions);
  }

  getBook(id:number):Observable<Book>{
    let strParams ='id='+id;

    const httpOptions = {
      headers: new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + this.publicbooksAPIkey),
      params: new HttpParams({fromString: strParams})
    };
    return this.http.get<Book>(this.bookUrl, httpOptions)
  }

  publicBooks(filter:string, limit:number, offset:number):Observable<Book[]>{

    let strParams ='filter='+filter+'&limit='+limit+'&offset='+offset;
    const httpOptions = {
      headers: new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + this.publicbooksAPIkey),
      params: new HttpParams({fromString: strParams})
    };

    
    let res = this.http.get<Book[]> (this.publicBooksUrl,httpOptions).pipe(map(
      (data:any) => {
         if(data == undefined)
            return new Array<Book>();
         if(data instanceof Array)
         {
           return data;
         } else {
            let array =new Array<Book>();
            array.push(data);
            return array;
         }

      }));
         return res;
  }

  countPrivateBooks(user:string, filter:string) {
    let strParams = 'user='+user+'&filter='+filter;
    const httpOptions = {
      headers: new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + this.auth.getAccessToken()),
      params: new HttpParams({fromString: strParams})
    };
    return this.http.get(this.countPrivateBooksUrl,httpOptions);
  }

  privateBooks(user:string,filter:string, limit:number, offset:number):Observable<Book[]>{

    let strParams = 'user='+user+'&filter='+filter+'&limit='+limit+'&offset='+offset;
    const httpOptions = {
      headers: new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + this.auth.getAccessToken()),
      params: new HttpParams({fromString: strParams})
    };

    
    let res = this.http.get<Book[]> (this.privateBooksUrl,httpOptions).pipe(map(
      (data:any) => {
         if(data == undefined)
            return new Array<Book>();
         if(data instanceof Array)
         {
           return data;
         } else {
            let array =new Array<Book>();
            array.push(data);
            return array;
         }

      }));
         return res;
  }

  addBook(book:Book) {
    const httpOptions = {
      headers: new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + this.auth.getAccessToken())
    };
    return this.http.post(this.bookUrl,book,httpOptions);
  }

  deleteBook(id:number) {
    const httpOptions = {
      headers: new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + this.auth.getAccessToken())
    };
    return this.http.delete(this.bookUrl + '/'+id, httpOptions);
  }

  updateBook(book:Book) {
    const httpOptions = {
      headers: new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + this.auth.getAccessToken())
    };
    return this.http.put(this.bookUrl,book,httpOptions);
  }

}
