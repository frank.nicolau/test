import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ImgService {
  private uploadUrl:string = environment.imgApiUrl+ '/upload';
  private updateUrl:string = environment.imgApiUrl+ '/update';

  constructor(private http: HttpClient, private auth:AuthService) { 

  }

  upload(img:File, bookId: number) {
    const httpOptions = {
      headers: new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.auth.getAccessToken())
    };
    
    const formData = new FormData();
    formData.append('bookId', String(bookId));
    formData.append('imageFile', img, img.name);
    return this.http.post(this.uploadUrl,formData,httpOptions);
  }

  update(img:File, bookId: number) {
    const httpOptions = {
      headers: new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.auth.getAccessToken())
    };
    
    const formData = new FormData();
    formData.append('bookId', String(bookId));
    formData.append('imageFile', img, img.name);
    return this.http.post(this.updateUrl,formData,httpOptions);
  }
  
}
