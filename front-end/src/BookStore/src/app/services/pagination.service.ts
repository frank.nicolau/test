import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaginationService {

  limit:number;
  currentPage:number;
  private offSub = new Subject<number>();
  private tpagSub = new Subject<number>();

  constructor() {
     this.limit = 9;
     this.currentPage = 0;
   }

    getlimit():number {
      return this.limit;
    }
    
    offsetChange():Observable<number>{
     return this.offSub.asObservable();
   }

   totalPageChange():Observable<number>{
    return this.tpagSub.asObservable();
   }

    updateTotal(total:number){
       if(total < this.limit)
           this.tpagSub.next(1);
        else {
          
          if(total%this.limit == 0) {
            this.tpagSub.next(Math.floor(total/this.limit));
          } else {
             this.tpagSub.next(Math.floor(total/this.limit)+1);
          }
        }
    }

    updateOffset(page:number) {
      --page;

      if(this.currentPage == page)
      return;
         this.offSub.next((this.limit*page));   
        this.currentPage = page;
    }
}
