import { Injectable } from '@angular/core';
import { OidcSecurityService, OpenIdConfiguration } from 'angular-auth-oidc-client';
import { environment } from 'src/environments/environment';
import { NavigationService } from './navigation.service';
import { User } from '../models/user';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({providedIn: 'root'}) //
export class AuthService {

  private isAuthenticated = false;
  private apiManagerTokenUrl:string = environment.oauth2Configuration.tokenUrl;
  private apiManagerClientId:string = environment.oauth2Configuration.clientId;
  private apiManagerSecret:string = environment.oauth2Configuration.clientSecret;
  private accessToken:string = "";
  
  constructor(public oidcSecurityService: OidcSecurityService, 
    private httpclient: HttpClient, private navigationService: NavigationService) {

    this.oidcSecurityService.checkAuth().subscribe((auth) =>{
        console.log(auth);
    });

    this.oidcSecurityService.userData$.subscribe(data =>{
        localStorage.setItem('currentUser', JSON.stringify(data));  
    });

    this.oidcSecurityService.isAuthenticated$.subscribe(data=>{
       this.isAuthenticated = false;
       
        if(data === true)  {
          this.getOauth2Token().subscribe((data:any) => {
            this.accessToken = data.access_token;
            this.isAuthenticated = true;
            this.navigationService.updateSideBar(true);
          })
        } else{
          this.navigationService.updateSideBar(false);
        } 
    });
   }


  login(): void {
      this.oidcSecurityService.authorize();
  }

   logout(): void{
      this.oidcSecurityService.logoff();
      localStorage.removeItem('currentUser');
   }

   userName(): string {
      let currentUser: User = JSON.parse(localStorage.getItem("currentUser"));
      return currentUser.sub;
   }

  isAuth() {
    return this.isAuthenticated;
  }

  getAccessToken():string {
    return this.accessToken;
  }

  getOauth2Token() {
    const body = new HttpParams()
    .set('grant_type', 'urn:ietf:params:oauth:grant-type:jwt-bearer')
    .set('assertion', this.oidcSecurityService.getIdToken());

    const base64 = window.btoa(this.apiManagerClientId+':'+this.apiManagerSecret);

    const httpOptions = {
      headers: new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization','Basic '+base64)
    };

    return this.httpclient.post(this.apiManagerTokenUrl,body.toString(),httpOptions);
  }
}
