import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private filter = new Subject<string>();
  private filterValue = "";
  constructor() { 

  }

  getFilterValue():string{
  return this.filterValue;
  }

  search(value: string) {
     this.filter.next(value);
  }
 
   valueChange():Observable<string> {
    return this.filter.asObservable();
  }
   
}
