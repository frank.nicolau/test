import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Router, NavigationStart } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MsgService {
  private subject = new Subject<any>();
  private keepAfterNavigationChange = false;

  constructor(private router: Router) { 
    
     this.router.events.subscribe(event=>{
        if(event instanceof NavigationStart)
          {
            if(this.keepAfterNavigationChange) {
               //only keep for a single location change
              this.keepAfterNavigationChange = false;
            } else {
              //clear alert
               this.subject.next();
            }
          }

     })
  }

  success(message:string, keepAfterNavigationChange = false, autoClose= false){
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.subject.next({type:'success', text:message});
    if(autoClose) {
       setTimeout(() => this.setMessage(), 3000);
    }
  }

  error(message:string, keepAfterNavigationChange = false,  autoClose= false){
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.subject.next({type:'error', text:message});
    if(autoClose) {
      setTimeout(() => this.setMessage(), 2000);
   }
  }

  setMessage(){
    this.subject.next(null);
  }

  getMessage():Observable<any> {
     return this.subject.asObservable();
  }
}
