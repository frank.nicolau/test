import { Injectable } from '@angular/core';
import { Navitem } from '../models/navitem';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  private items = new Subject<Navitem[]>();

  constructor() { 
    
  }

  itemsChange():Observable<Navitem[]>{
    return this.items.asObservable();
  }

  setItems(items:Navitem[]):void{
     this.items.next(items);
  }
}
