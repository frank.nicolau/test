import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FtpService {

  private uploadUrl:string = environment.ftpApiUrl+ '/upload';
  private downloadUrl:string = environment.ftpApiUrl+ '/download';
  private deleteUrl:string = environment.ftpApiUrl+ '/delete';

  constructor(private http: HttpClient, private auth:AuthService) { }

  upload(file:File) {
    const httpOptions = {
      headers: new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.auth.getAccessToken())
    };
    
    const formData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post(this.uploadUrl,formData,httpOptions)
  }

  download(fileUri:string) {
      let strParams = 'fileUri='+fileUri;
      let headers= new HttpHeaders();
      headers = headers.set('Authorization', 'Bearer ' + this.auth.getAccessToken());
      headers = headers.set('Accept','application/pdf');
      const params =new HttpParams({fromString: strParams});
      return this.http.get(this.downloadUrl,{headers,params,responseType:'blob'})
  }

  deleteFile(fileUri:string) {
    const httpOptions = {
      headers: new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + this.auth.getAccessToken())
    };
    return this.http.delete(this.deleteUrl + '/'+fileUri, httpOptions);
  }
}
