import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Navitem } from '../models/navitem';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  private currentItem = new Subject<Navitem>();
  private hideNavBar = new Subject<boolean>()

  constructor() {
  }

  navItemChange(): Observable<Navitem>{
    return this.currentItem.asObservable();
  }

  setNavItem(item:Navitem) {
     this.currentItem.next(item);
  }

   hide():Observable<boolean> {
     return this.hideNavBar.asObservable();
   }

   setHide(hide:boolean){
     this.hideNavBar.next(hide); 
   }

}
