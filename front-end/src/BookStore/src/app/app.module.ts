import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { ContactComponent } from './components/contact/contact.component';
import { PrivateBookComponent } from './components/private-book/private-book.component';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { ForbiddenComponent } from './components/forbidden/forbidden.component';
import { AddComponent } from './components/forms/add/add.component';
import { EditComponent } from './components/forms/edit/edit.component';
import { ProfileComponent } from './components/profile/profile.component';
import { MsgComponent } from './components/msg/msg.component';
import { DetailsBookComponent } from './components/details/details-book/details-book.component';
import { DetailsViewComponent } from './components/details/details-view/details-view.component';
import { AuthModule, LogLevel, OidcConfigService } from 'angular-auth-oidc-client';
import { environment } from 'src/environments/environment';
import { ErrorInterceptor } from './interceptors/error.interceptor';


export function configureAuth(oidcConfigService: OidcConfigService) {
  return () =>
      oidcConfigService.withConfig(environment.openIdConfiguration);
}

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    WelcomeComponent,
    ContactComponent,
    PrivateBookComponent,
    UnauthorizedComponent,
    ForbiddenComponent,
    AddComponent,
    EditComponent,
    ProfileComponent,
    MsgComponent,
    DetailsBookComponent,
    DetailsViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AuthModule.forRoot()
  ],
  providers: [
    OidcConfigService,
    {
        provide: APP_INITIALIZER,
        useFactory: configureAuth,
        deps: [OidcConfigService],
        multi: true,
    },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// I have cicle dependency error when include HTTP_INTERCEPTORS with AUTH SERVICE
// { 
//   provide: HTTP_INTERCEPTORS,
//   useClass: AuthInterceptor,
//   multi: true
// },